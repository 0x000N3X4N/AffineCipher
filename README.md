# AffineCipher

> https://en.wikipedia.org/wiki/Affine_cipher

- ../exmpl/msg.txt contains the message to be encrypted/decrypted

- ../exmpl/encryptedMsg.txt contains a valid encrypted message

- ../exmpl/decryptedMsg.txt contains a valid decrypted message

@template: ./AffineCipher [a] [b] e/d

@example: ./AffineCipher 21 8 e

